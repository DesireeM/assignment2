//Desiree Mariano 2236344
package application;
import vehicles.Bicycle;

public class BikeStore 
{
    public static void main (String [] args)
    {
        Bicycle[] b = new Bicycle[4];

        b[0] = new Bicycle("Specialized", 21, 40.0);
        b[1] = new Bicycle("Ford Motor Company", 25, 45.0);
        b[2] = new Bicycle("General Electric", 30, 50.0);
        b[3] = new Bicycle("Michelin", 35, 55.0);
        
        
        for(int i = 0; i < b.length; i ++)
        {
            System.out.println(b[i]);
        }
    }
}
